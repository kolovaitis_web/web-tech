<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Знаёмствы</title>
    <link rel="shortcut icon" href="favicon.png" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Bad+Script|Pattaya&display=swap&subset=cyrillic"
          rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="body-image-background">
<div class="main">
    <div class="header">
        <div class="logo_big">
            <h1 class="logo_text">Знаёмствы</h1>
        </div>
    </div>
    <?php
    $page = $_GET['page']; ?>
    <div class="content_main">
        <div class="navigation_container">
            <a href="main.php?page=anket">
                <div class="nav_item <?php if ($page == 'anket') {
                    echo 'choosed_nav';
                } else {
                    echo 'unchoosed_nav';
                } ?>"><img class="nav_img " src="images/nav/anket.png"/></div>
            </a>
            <a href="main.php?page=profile">
                <div class="nav_item <?php if ($page == 'profile') {
                    echo 'choosed_nav';
                } else {
                    echo 'unchoosed_nav';
                } ?>"><img class="nav_img " src="images/nav/profile.png"/></div>
            </a>

            <a href="main.php?page=settings">
                <div class="nav_item <?php if ($page == 'settings') {
                    echo 'choosed_nav';
                } else {
                    echo 'unchoosed_nav';
                } ?>"><img class="nav_img " src="images/nav/settings.png"/></div>
            </a>
        </div>
        <?php
        switch ($page) {
            case 'anket':
                require_once 'anket.php';
                break;
            case 'settings':
                require_once 'settings.php';
                break;
            case 'profile':
                require_once 'profile.php';
                break;
        }
        ?>
    </div>
</div>
</body>
</html>