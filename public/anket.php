<link rel="stylesheet" type="text/css" href="styles/style.css">
<link rel="stylesheet" type="text/css" href="styles/anket.css">
<div class="anket_container">
    <div class="card">
        <img class="anket_image" src="images/woman.jpg"/>
        <div class="action_button cancel"></div>
        <div class="action_button accept"></div>
    </div>
</div>
<div class="summary_container">
    <div class="anket_name big_text white"><h1>Дина 21</h1></div>
    <div class="common_text info">
        <div class="city"><h3>Город: Минск</h3></div>
        <div class="summary_text"><p>Люблю котиков и кино. Поболтаем?</p></div>
    </div>
</div>
   