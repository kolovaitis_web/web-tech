<link rel="stylesheet" type="text/css" href="styles/style.css">
<link rel="stylesheet" type="text/css" href="styles/anket.css">
<link rel="stylesheet" type="text/css" href="styles/settings.css">
<div class="settings_container">
    <form>
        <div class="setting gender_setting">
            <div class="setting_label">Шукаць:</div>
            <div class="setting_option common_text">
                <input type="radio" class="radio" name="gender" id="radio_male"/>
                <label for="radio_male">Мужчын</label>
                <input type="radio" class="radio" name="gender" id="radio_female" checked="checked"/>
                <label for="radio_female">Жанчын</label>
                <div class="gender_image_container">
                    <img src="images/male.png" class="male_image gender_image">
                    <img src="images/female.png" class="female_image gender_image">
                </div>
            </div>
        </div>
        <div class="setting_option common_text">
            <label for="checkbox_city">Толькі ў маім горадзе</label>
            <input type="checkbox" class="checkbox" name="city" id="checkbox_city"/>
        </div>
        <div class="setting">
            <div class="setting_label">Узрост:</div>
            <div class="setting_option common_text">
                <label for="min_old">Ад</label>
                <input type="number" value="18" class="old_input" id="min_old"/>
                <label for="max_old">Да</label>
                <input type="number" value="22" class="old_input" id="max_old"/>
            </div>
        </div>
    </form>
</div>
