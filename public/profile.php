<link rel="stylesheet" type="text/css" href="styles/style.css">
<link rel="stylesheet" type="text/css" href="styles/anket.css">
<link rel="stylesheet" type="text/css" href="styles/profile.css">
<div class="about_me_container">
    <div class="anket_name big_text white"><h1>Эрлинг Холланд 19</h1></div>
    <div class="common_text text">
        <div class="city"><h3>Город: Дортмунд</h3></div>
        <div class="summary_text"><p>Да, я и в правду похож на Никиту Коловайтиса)</p></div>
    </div>
    <div class="change_button_container">
        <input type="button" class="button change_profile" value="Змяніць апісанне"/>
        <input type="button" class="button change_photo" value="Змяніць фотаздымак"/>
    </div>
</div>
<div class="my_photo_container">
    <img src="images/me.jpg" class="my_photo">
</div>
